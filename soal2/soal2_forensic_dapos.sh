DIR=forensic_log_website_daffainfo_log
if [ ! -d "$DIR" ]; then
	mkdir $DIR
else
	:
fi

info=$(awk '
BEGIN{FS=":"}
NR==2{printf "%d %d %d\n", $3, $4, $5}
END{printf "%d %d %d %d", $3, $4, $5, NR-1}' log_website_daffainfo.log)

data=(${info//:/ })
calcH=$(($((${data[3]}-1))-${data[0]}))
calcM=$(($((${data[4]}+59))-${data[1]}))
calcS=$(($((${data[5]}+60))-${data[0]}))
jam=$(($calcH+$(($(($calcM/60))+$(($calcS/3600))))))
req=${data[6]}
avg=$(($req/$jam))

msg="Rata-rata serangan adalah sebanyak $avg requests per jam" 
echo $msg > forensic_log_website_daffainfo_log/ratarata.txt

echo -e "Point c belum terdevelop\n" > forensic_log_website_daffainfo_log/result.txt

awk '
	BEGIN{FS=":"} /curl/{flag++}
	END{print "Ada %d requests yang menggunakan curl sebagai user-agent", flag}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

awk '
	BEGIN{FS=":"}
	{if($3=="02") printf "\n%s", $1}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
