# soal-shift-sisop-modul-1-IT03-2022
Laporan pengerjaan soal shift modul 1 Praktikum Sistem Operasi 2022 Kelompok ITA03

## Anggota Kelompok:
1. Salsabila Briliana Ananda Sofi - 5027201003
2. Rafif Naufaldi Wibowo - 5027201010
3. Fairuz Azka Maulana - 5027201017

## Soal 1
## study case soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

## problem soal 1
a. membuat sistem register yang harus tedapat file register.sh dan main.sh yang dimana file segister.sh harus memiliki penyimpanan data setiap user yang telah melakukan register yang disimpan pada direktori ./users/user.txt.

b. membuat persyaratan dalam pembuatan pasword yakni membuat password berbeda dengan username, menggunakan seminimalnya 8 character,menggunakan angka, menggunakan huruf besar, dan huruf kecil.

c. mencatat waktu login yang terdapat di script main.sh yang dimana data waktu tersebut disimpan pada log.txt dan memiliki pesan untuk tentang keberhasilan dan kegagalan login dan register.

d. menghitung jumlah percobaan dalam login dan mendownload gambar yang akan disimpan file zip.

## solution soal 1 A
[Source Code](https://gitlab.com/RafifWibowo/soal-shift-sisop-modul-1-it03-2022/-/blob/main/soal1/register.sh)
```
#!/bin/bash
log_path="log.txt"
dir=./users
db_path=./users/user.txt
if [ -d "$dir" ]; then
	:
else
	mkdir ./users
	touch ./users/user.txt
fi
```
Langkah pertama dyaitu membuat *register.sh* terlebih dahulu yang kemudian dalam script tersebut praktikan membuat variable log_path dan db_path yang akan merujuk pada file log.txt dan user.txt kemudian terdapat perintah yang membuat folder users dan yang kemudian folder tersebut menjadi sebuah wadah untuk menampung file *user.txt* yang dimana file tersebut sebagai media penyimpanan dari data yang akan dimasukan. Untuk membuat register membuat variabel password dan username dan dibutuh kan sebuah fungsi *read* sebagai pembaca username dan password yang dimasukan. 
```
#A
read -p "username: " username
read -s -p "password: " password

data=$(awk -v var="$username" '$1~var{print $1}' $db_path)
if [ -n "$data" ]
then
	echo -e "\n$username exist!"
    message="REGISTER: ERROR User already exists $username"
    echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
    exit 1
fi 

```
kemudian menggunakan awk sebagai command dari variable data yang dimana di dalam awk tersebut akan melakukan pengecekan yang diambil dari variable username dari db_path. Jika variable username tersebut sudah ada di dalam file user.txt maka akan terdapat pesan "REGISTER: ERROR User already exists” juka username tersebut belum terdapat pada db_path maka akan dicatat format waktu yang sudah tertera sebagai bulan /hari/tahun jam/menit/detik pada log_path. Kemudian akan berlanjut pada proses berikutnya melalui exit 1.

## solution soal 1 B
```
#B
echo -e "\n"
checkpass(){
    # $1 = username $2 = password
    if  [ $1 == $2 ]
        then echo -e "Requirment 1 is not enough\n"
        return 0
    elif [ ${#2} -lt 8 ]
        then echo -e "Requirment 2 is not enough\n"
        return 0
    else 
        if [[ $2 =~ ^[[:alnum:]]+$ ]];then
			:
                else
                        echo -e "Requirement is not Enough\n"
                        return 0
	fi
    fi
    echo $1 $2 >> $db_path
    message="REGISTER: INFO User $username registered successfully"
    echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
}
checkpass $username $password
```
Dari proses yang sudah dilakukan pada soal a cript akan dilanjutkan pada pengecekan password  menggunakan checkpass. Langkah yang pertama menandakan $1 sebagai username dan $2 sebagai password kemudian password akan dicek pada requirement pertama dengan $1 dan $2 tidak boleh sama jika terdeteksi sama maka akan terdapat pesan “Requirment 1 is not enough” dan akan dikembalikan Kembali pada proses awal. Jika requirement sudah terpenuhi maka akan berlanjut pada pengecekan berikutnya yaitu password harus memiliki setidaknya delapan karakter menggunakan fungsi -lt 8 jika karakter pada password tidak memenuhi minimal jumlah karakter maka akan terdapat pesan “Requirment 2 is not enough” dan akan dikembalikan Kembali pada proses awal. Jika requirement sudah terpenuhi maka akan melanjukan pengecek password yaitu password harus memiliki  huruf kapital, huruf, dan angka dengan menggunakan perintah (^)dan alnum. Kemudian username dan password yang sudah diberikan message “REGISTER: INFO User (username yang dimasukan) registered successfully”dan waktu register akan dicatat dalam log.txt.

## solution soal 1 C
[Source Code](https://gitlab.com/RafifWibowo/soal-shift-sisop-modul-1-it03-2022/-/blob/main/soal1/main.sh)
```sh
#!/bin/bash

log_path=./log.txt
db_path=./users/user.txt

read -p "username: " username
read -s -p "password: " password

user=$(grep "$username" $db_path)

if [ "$user" != "" ]; then
    correct_pass="$(echo $user | awk '{print $2}')"
    if [ $correct_pass != $password ]; then
        message="LOGIN: ERROR Failed login attempt on user $username"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
        exit 1
    elif [ $correct_pass == $password ]; then
    	message="LOGIN: INFO User $username logged in"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
    fi
fi
```
Pada proses login ini username dan password yang berada pada user.txt akan diambil untuk pengecekan dalam variabel user dengan perintah grep yang mengacu pada db_path. Kemudian variabel user akan dijadikan sebagai correct_pass dengan menggunakan awk yang mengambil kata kedua yakni password. Kemudian ketika praktikan ingin melakukan login pada saat memasukan password, password tersebut akan dilakukan perbandingan dengan correct_pass yang kemudian jika salah maka akan dikirimkan pesan "LOGIN: ERROR Failed login attempt on user $username" yang kemudian juga dicatat waktu dari percobaan login tersebut di dalam log.txt kemudian jika password yang dimasukan sudah benar maka proses tersebut akan dilanjutkan dengan massage "LOGIN: INFO User $username logged in" yang kemudia waktu login nya akan dicatat di dalam log.txt.

## solution soal 1 D att
```sh
echo -e "\n"
read -p "Insert command (dl, att): " command

case "$command" in
	"dl")
		echo -e "command belum di develop\n"
		;;
	"att")
		successLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($6 == var) n+=1} END{print n-1}' log.txt)
		failedLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($NF == var) n+=1} END{print n}' log.txt)
		#echo -e "$successLog $failedLog\n"
		sum=$(($successLog+$failedLog))
		echo -e "Attempts: $sum \n"
		;;
	*)
		echo -e "Command doesn't exist!"
		exit 1
		;;
	esac
```
Pada soal ini diperlukan case untuk melakukan command untuk melakukan dl dan att. Pada bagian att praktikan harus mengetahui berapa kali percobaan login yang dilakukan dengan cara membuat variabel successLog dan fieldLog menggunakan awk untuk mengambil data username yang dicoba dimasukan pada log.txt. Yang dimana successLog akan dimulai dari 0 hitungan percobaan nya menggunakan BEGIN{n=0} kemudian menggunakan $6 untuk mendeteksi username yang berhasil login dikarenakan $username terletak pada kata ke-6 yang kemudian akan ditambah kan sebagai 1 percobaan login yang berhasil. Kemudian failedLog akan juga dimulai dari 0 hitungan percobaan nya menggunakan BEGIN{n=0} kemudian menggunakan $NF untuk mendeteksi username yang gagal login dikarenakan $username terletak pada kata terakhir yang kemudian akan ditambah kan sebagai 1 percobaan login yang gagal. Kemudian menggunakan sum untuk variabel perintah menambahkan successLog dan failedLog. Kemudian akan diketahui jumlah percobaan login yang dilakukan.

## kendala
pada soal 1 terdapat kendala pada bagian soal d yaitu bagian dl untuk melakukan download gambar yang akan disimpan pada file zip


## Soal 2
### Study Case
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info

### Problem
- Membuat folder forensic_log_website_daffainfo_log untuk menyimpan hasil.
- Mencari rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
- Mencari IP yang paling banyak melakukan request ke server dan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya. 
- Mencari berapa banyak requests yang menggunakan user-agent curl. Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
- Mencari daftar IP yang mengakses website pada jam 2 pagi. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

### Solution
[Source Code](https://gitlab.com/RafifWibowo/soal-shift-sisop-modul-1-it03-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)

Soal pada poin A yaitu membuat folder dengan nama forensic_log_website_daffainfo_log. 

```sh
DIR=forensic_log_website_daffainfo_log
if [ ! -d "$DIR" ]; then
	mkdir $DIR
else
	:
```

Pertama yaitu mendeklarasikan DIR dengan nama forensic_log_website_daffainfo_log kemudian cek menggunakan fungsi if else apakah DIR sudah dibuat, jika belum maka make directory, jika directory sudah ada maka lanjut ke poin B.

Pada poin B diminta untuk menampilkan jumlah rata-rata request per jam. 

```sh
info=$(awk '
BEGIN{FS=":"}
NR==2{printf "%d %d %d\n", $3, $4, $5}
END{printf "%d %d %d %d", $3, $4, $5, NR-1}' log_website_daffainfo.log)
```

Menggunakan fungsi awk kemudian format log pada file yang sudah disediakan akan dipisahkan dengan (:). Kemudian pengecekan akan dimulai pada baris kedua karena informasi request dimulai dari baris kedua. Kemudian print kolom ketiga, keempat, dan kelima dari format log yang sudah dipisah-pisahkan, yaitu kolom untuk jam, menit, dan detik. Setelah itu pada bagian END, selain print kolom ketiga, keempat, kelima juga mengurangi baris saat ini dengan 1 atau NR-1 untuk mengetahui request berada pada posisi berapa. Data request diambil dari file dengan nama log_website_daffainfo.log dan disimpan pada variable (info).

```sh
data=(${info//:/ })
calcH=$(($((${data[3]}-1))-${data[0]}))
calcM=$(($((${data[4]}+59))-${data[1]}))
calcS=$(($((${data[5]}+60))-${data[0]}))
```

Kemudian data pada variabel (info) akan diubah menjadi array dan disimpan pada variabel (data). Kemudian menghitung waktu dengan mengurangkan waktu pada request baris terakhir dengan waktu pada request baris awal.

Awal : "22/Jan/2022:00:11:10"
Akhir : "22/Jan/2022:12:00:00"

Namun karena waktu pada baris terakhir tidak bisa langsung dikurangkan dengan waktu pada baris awal, maka agar sesuai jam pada baris terakhir akan dikurangi 1 lalu kolom menit akan ditambah 59 dan kolom detik akan ditambah 60. Setelah itu baru bisa dikurangkan dengan waktu pada ruquest baris awal.

```sh
jam=$(($calcH+$(($(($calcM/60))+$(($calcS/3600))))))
req=${data[6]}
avg=$(($req/$jam))
```

Kemudian hasil pengurangan jam, menit, dan detik akan dikonversikan menjadi jam lalu ditambah. Untuk mengubah menit menjadi jam maka akan dibagi dengan 60 lalu untuk detik akan dibagi dengan 3600. Setelah itu menghitung jumlah request dan disimpan pada variabel (req), kemudian untuk menghitung nilai rata-rata, jumlah request dibagi dengan jam lalu disimpan pada variabel (avg).

```sh
msg="Rata-rata serangan adalah sebanyak $avg requests per jam" 
echo $msg > forensic_log_website_daffainfo_log/ratarata.txt
```

Kemudian mendeklarasikan pesan "Rata-rata serangan adalah sebanyak $avg requests per jam" lalu pesan tersebut akan disimpan pada directory forensic_log_website_daffainfo_log pada file ratarata.txt. Hasilnya yaitu sebagai berikut:

![rata-rata](img/ratarata.png)

Pada pint D diminta untuk menghitung jumlah request yang menggunakan user-agent curl.

```sh
awk '
	BEGIN{FS=":"} /curl/{flag++}
	END{print "Ada %d requests yang menggunakan curl sebagai user-agent", flag}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

Pertama yaitu menggunakan awk lalu memisahkan format log menggunakan (:). Setelah itu menggunakan /curl/ untuk mencari request yang mengandung kata curl lalu flag akan bertambah setiap menemukan request dengan curl. Kemudian print "Ada %d requests yang menggunakan curl sebagai user-agent" dengan hasil yang disimpan pada flag. Setelah itu hasil tersebut akan ditambahkan pada file result.txt yang telah dibuat pada directory forensic_log_website_daffainfo_log. 

Pada poin E diminta untuk mencari daftar IP yang mengakses website pada jam 2 pagi pada tanggal 22.

```sh
awk '
	BEGIN{FS=":"}
	{if($3=="02") printf "\n%s", $1}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

Menggunakan fungsi awk lalu memisahkan format log menggunakan (:). Kemudian jika pada kolom ketiga yaitu kolom jam sama dengan 02 maka print atau cetak kolom 1 atau kolom IP. Setelah daftar IP telah ditemukan akan ditambahkan pada file result.txt yang telah dibuat pada directory forensic_log_website_daffainfo_log. Untuk hasil pada file result.txt yaitu sebagai berikut:

![Result](img/result.png)

### Kendala
Kendala yang dialami pada soal nomor 2 yaitu praktikan masih belum bisa menyelesaikan persoalan pada poin c yaitu, mencari IP yang paling banyak melakukan request ke server dan berapa banyak request yang dikirimkan dengan IP tersebut, hingga waktu pengerjaan soal shift berakhir.

## Soal 3
### Study Case
---
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. 

### Problem
---
- Membuat script monitoring RAM dan Size suatu directory, directory yang akan di monitor adalah /home/{user}.
- Setiap script dijalankan, semua metric yang ada dimasukkan ke dalam sebuah file log bernama **metric_{YmdHms}.log**
    - YmDHms adalah timestamp saat script dijalankan
- Contoh isi dari file log yang dihasilkan adalah sebagai berikut:
    ```
    mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
    15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
    ```
- Script tersebut diharapkand dapat dijalankan setiap menit secara otomatis.
- Kemudian buat satu script lagi untuk membuat agregrasi file log ke satuan jam.
    - Script agregasi akan memiliki info dari file-file yang dibuat tiap menit.
    - Contoh isi dari file agregasi yang dihasilkan adalah sebagai berikut:
    ```
    type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
    minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
    maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
    average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62
    ```
- File agregasi diharapkan dapat dijalankan secara otomatis setiap satu jam
- Format nama file agregasi adalah **metric_agg_{YmdH}.log**
    - YmdH adalah timestamp saat script dijalankan
- Semua file log terletak pada /home/{user}/log
- Script yang dijalankan tiap menit bernama **minute_log.sh**
- Script yang dijalankan tiap menit bernama **aggregate_minute_to_hourly.sh**


### Solution
---
[Source Code](https://gitlab.com/RafifWibowo/soal-shift-sisop-modul-1-it03-2022/-/blob/main/soal3/minute_log.sh)

Pada script **minute_log.sh**, pertama cek terlebih dahulu directory tempat file log akan tergenerate sudah ada atau belum menggunakan if else
```bash
DIR=/home/kali/log
if [ ! -d "$DIR" ]; then
	mkdir /home/kali/log
else
	:
fi
```
Bila directory tidak ada maka akan menjalankan command pembuatan directory.

Kemudian, untuk monitoring ram dan size directory kami menggunakan command linux, yaitu `free -m` dan `du -sh /home/kali`. Untuk menjalankannya didalam script kami menggunakan eval dan hasilnya kami manipulasi ke dalam bentuk array dan ditampung pada variabel **$dataRam** dan **$dataStorage**.
```bash
ram=$(eval "free -m")
storage=$(eval "du -sh /home/kali")
dataRam=(${ram//:/ })
dataStorage=(${storage//:/ })
```

![Hasil Monitoring](img/monitoring.png)

Unutk mengambil data monitoring yang sesuai pada program, kami ambil datanya sesuai dengan index array (angka warna merah) pada gambar diatas dan disimpan pada variabel seperti pada code berikut.
```bash
mem_total=${dataRam[7]}
mem_used=${dataRam[8]}
mem_free=${dataRam[9]}
mem_shared=${dataRam[10]}
mem_available=${dataRam[12]}
swap_total=${dataRam[14]}
swap_used=${dataRam[15]}
swap_free=${dataRam[16]}
path=${dataStorage[1]}
path_size=${dataStorage[0]}
```

Setelah itu, untuk mendapatkan timestamp untuk format nama file log, kami menggunakan date pada bash dan disimpan pada variable **$timeStamp**.
```bash
timeStamp=$(date +"%Y%m%d%H%M%S")
```

Selanjutnya, tiap variable yang menyimpan data monitoring diformat dan disimpan pada variable **$log**. Isi dari variable **$log** inilah yang akan menjadi isi dari file log yang tergenerate.
```bash
log="$mem_total,$mem_used,$mem_free,$mem_shared,$mem_available,$swap_total,$swap_used,$swap_free,$path,$path_size"
echo "$log" > /home/kali/log/metrics_$timeStamp.log
```

Untuk memungkinkan script ini berjalan berjalan otomatis tiap menit, kami menggunakan cron dengan file sebagai berikut.
```
*/1 * * * * bash /home/kali/soal-shift-sisop-modul-1-it03-2022/soal3/minute_log.sh
```

Untuk hasilnya adalah sebagai berikut:

![Isi Folder Log](img/folderLog.png)
![Isi File Log](img/isiLog.png)

### Kendala
---
Pada soal 3 ini, praktikan mengalami kendala untuk memahami algoritma yang akan digunakan untuk pembuatan script untuk bagian agregasi tiap jam. Sehingga praktikan belum dapat mengerjakan bagian agregasi sampai waktu pengerjaan soal shift selesai

## Revisi
> Perubahan yang dilakukan setelah pengerjaan soal shift
### 1. Command DL pada main.sh
```sh
num="${Arr[1]}"
                init_num=0;
                dir_date=$(date '+%Y-%m-%d')
                path_dir=$dir_date"_$username"
                FILE=$path_dir".zip"
                if [[ -f "$FILE" ]]
                then
                    eval "unzip -P $password $FILE"
                   
                    init_num=$(eval "ls $path_dir/ | wc -l")
                else
                    eval "mkdir $path_dir"
                fi
                total=$((init_num+num))
                for ((i=init_num+1; i<=total; i++))
                do
                    img_path=$path_dir"/PIC_$i"
                    wget_msg="wget -O $img_path https://loremflickr.com/320/240"
                    eval $wget_msg
                done
                zip_msg="zip -r -P $password $path_dir $path_dir"
                eval $zip_msg
                eval "rm -rf $path_dir"
```
script dari [ -f  “$FILE” ]] digunakan untuk mengecek file yang ada pada path yang kemudian dengan if akan memperoleh proses extect file zip pada gambar menggunakan eval "unzip -P $password $FILE" kemudian file akan dihitung didalam folder hasil extract dengan $(eval "ls $path_dir/ | wc -l") yang nanti akan disimpan kedalam init_num. kemudian dengan variabel total yang sebagaimana gambaran dari dalam folder dengan menggunakan for sebagai loop indeks nya. kemudian variabel img_path yang difungsikan sebagai path dan nama format dari fambar sedangkan wget_msg berfungsi untuk mendownload gambar pada -0 $img_path dan eval sebagai untuk menjalankan command wget_msg. kemudan dengan zip_msg="zip -r -P $password $path_dir $path_dir" untuk melakukan zip folder menggunakan $path_dir kemudian password menggunakan -P $password yang berikutnya akan disimpan pada file $path_dir. kemudian zip_msg="zip -r -P $password $path_dir $path_dir" berfungsi menghapus folder path_dir dan isinya.

### 2. IP paling sering melakukan request pada soal no. 2
---
Soal nomor 2 pada poin c yaitu mencari IP yang paling banyak melakukan request ke server dan berapa banyak request yang dikirimkan dengan IP tersebut. 

```sh
awk '
    BEGIN{FS=":"}
    {if(flag[$1]++ >= max) max = flag[$1]}
    END{
        for(x in flag) 
            if(max == flag[x]) 
                printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", x, flag[x] 
    }
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
```

Pertama yaitu menggunakan fungsi awk lalu memisahkan format log menggunakan (:). Kemudian dengan `{if(flag[$1]++ >= max) max = flag[$1]}` setiap menemui nomor IP yang sama maka nilai dari IP tersebut akan bertambah dan variabel max akan menyimpan nilai dari IP yang memiliki jumlah sama atau lebih besar dari nilai max. 

Selanjutnya pada `for(x in flag)` menunjukkan bahwa x adalah IP dalam array flag. Kemudian jika ditemukan jumlah nilai yang sama dengan nilai max yaitu jumlah nilai IP terbanyak maka akan mencetak `printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", x, flag[x]` yang akan disimpan pada directory forensic_log_website_daffainfo_log pada file result.txt.

### 3. File Agregasi
---
Pertama, kami buat script dengan nama **aggregate_minutes_to_hourly_log.sh**. Kemudian, untuk mendapatkan data tiap menit, kami membuat sebuah fungsi bernama readFile dengan isi sebagai berikut:
```bash
function readFile(){
for file in $(ls /home/kali/log/metrics_* | grep -v agg | grep $(date +"%Y%m%d%H")); 
    do cat $file; 
done
}
```
Pada fungsi tersebut, kami melakukan iterasi pada hasil command `ls /home/kali/log/metrics_*`. Tetapi tidak semua file akan terbaca, pada iterasi terdapat beberapa pipe, dimana `grep -v agg` akan membuat nama file yang mengandung "agg" tidak akan terbaca dan `grep $(date +"%Y%m%d%H")` hanya akan mengambil timestamp yang berdasarkan jam saat script dijalankan. Pada setiap iterasi akan menampilkan tiap isi dari file log per menit.

Kemudian, kami membuat variabel untuk mengolah tiap data yang didapatkan. Isi dari variabel ada hasil dari fungsi readFile dan ada pipe yang akan menjalankan AWK. Disini kami mengambil contoh pada variabel yang akan mengolah data mem_used.
```bash
mem_used=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$2
        }; 
    if($2>max) {
        max=$2
    };
    if($2<min) {
        min=$2
    };
    total+=$2; count+=1
} END {
    print min,max,total/count
}')
```

Disini kami menggunakan "," sebagai separator saat awk membaca file. Kami melakukan algoritma untuk mencari nilai minimum dan maximum pada `$2` dimana `$2` adalah data **mem_used**. Dan nilai average didapatkan dengan cara membagi total dan count. Dimana total adalah hasil sum dari semua data `$2` dan count adalah total data `$2` yang terbaca.

Kemudian akan dibuat variabel-variabel yang akan menampung nilai-nilai yang sudah diolah sebelumnya. Untuk mendapatkan nilai yang sesuai akan menggunakan awk untuk membaca variabel hasil olahan data dan akan mengambil argumen yang sesuai. Karena untuk nilai average desimalnya masih menggunakan ",", digunakan `tr ',' '.'` untuk mengubahnya menjadi "."

```bash
mem_used_min=$(echo $mem_used | awk '{print $1}' | tr ',' '.')
mem_used_max=$(echo $mem_used | awk '{print $2}' | tr ',' '.')
mem_used_avg=$(echo $mem_used | awk '{print $3}' | tr ',' '.')
```

Untuk data-data yang lain menggunakan cara yang sama. Yang berbeda hanya perlu menyesuaikan argumen yang dibaca oleh AWK.

```bash
mem_total=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$1
        }; 
    if($1>max) {
        max=$1
    };
    if($1<min) {
        min=$1
    };
    total+=$1; count+=1
} END {
    print min,max,total/count
}')

mem_total_min=$(echo $mem_total | awk '{print $1}' | tr ',' '.')
mem_total_max=$(echo $mem_total | awk '{print $2}' | tr ',' '.')
mem_total_avg=$(echo $mem_total | awk '{print $3}' | tr ',' '.')

mem_free=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$3
        }; 
    if($3>max) {
        max=$3
    };
    if($3<min) {
        min=$3
    };

    total+=$3; count+=1
} END {
    print min,max,total/count
}')

mem_free_min=$(echo $mem_free | awk '{print $1}' | tr ',' '.')
mem_free_max=$(echo $mem_free | awk '{print $2}' | tr ',' '.')
mem_free_avg=$(echo $mem_free | awk '{print $3}' | tr ',' '.')

mem_shared=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$4
        }; 
    if($4>max) {
        max=$4
    };
    if($4<min) {
        min=$4
    };
    total+=$4; count+=1
} END {
    print min,max,total/count
}')

mem_shared_min=$(echo $mem_shared | awk '{print $1}' | tr ',' '.')
mem_shared_max=$(echo $mem_shared | awk '{print $2}' | tr ',' '.')
mem_shared_avg=$(echo $mem_shared | awk '{print $3}' | tr ',' '.')

mem_buff=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$5
        }; 
    if($5>max) {
        max=$5
    };
    if($5<min) {
        min=$5
    };
    total+=$5; count+=1
} END {
    print min,max,total/count
}')

mem_buff_min=$(echo $mem_buff | awk '{print $1}' | tr ',' '.')
mem_buff_max=$(echo $mem_buff | awk '{print $2}' | tr ',' '.')
mem_buff_avg=$(echo $mem_buff | awk '{print $3}' | tr ',' '.')

mem_available=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$6
        }; 
    if($6>max) {
        max=$6
    };
    if($6<min) {
        min=$6
    };
    total+=$6; count+=1
} END {
    print min,max,total/count
}')

mem_available_min=$(echo $mem_available | awk '{print $1}' | tr ',' '.')
mem_available_max=$(echo $mem_available | awk '{print $2}' | tr ',' '.')
mem_available_avg=$(echo $mem_available | awk '{print $3}' | tr ',' '.')

swap_total=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$7
        }; 
    if($7>max) {
        max=$7
    };
    if($7<min) {
        min=$7
    };
    total+=$7; count+=1
} END {
    print min,max,total/count
}')

swap_total_min=$(echo $swap_total | awk '{print $1}' | tr ',' '.')
swap_total_max=$(echo $swap_total | awk '{print $2}' | tr ',' '.')
swap_total_avg=$(echo $swap_total | awk '{print $3}' | tr ',' '.')

swap_used=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$8
        }; 
    if($8>max) {
        max=$8
    };
    if($8<min) {
        min=$8
    };
    total+=$8; count+=1
} END {
    print min,max,total/count
}')

swap_used_min=$(echo $swap_used | awk '{print $1}' | tr ',' '.')
swap_used_max=$(echo $swap_used | awk '{print $2}' | tr ',' '.')
swap_used_avg=$(echo $swap_used | awk '{print $3}' | tr ',' '.')

swap_free=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$9
        }; 
    if($9>max) {
        max=$9
    };
    if($9<min) {
        min=$9
    };
    total+=$9; count+=1
} END {
    print min,max,total/count
}')

swap_free_min=$(echo $swap_free | awk '{print $1}' | tr ',' '.')
swap_free_max=$(echo $swap_free | awk '{print $2}' | tr ',' '.')
swap_free_avg=$(echo $swap_free | awk '{print $3}' | tr ',' '.')

path_size=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$11
        }; 
    if($11>max) {
        max=$11
    };
    if($11<min) {
        min=$11
    };
    total+=$11; count+=1
} END {
    print min,max,total/count
}')

path_size_min=$(echo $path_size | awk '{print $1}' | tr ',' '.')
path_size_max=$(echo $path_size | awk '{print $2}' | tr ',' '.')
path_size_avg=$(echo $path_size | awk '{print $3}' | tr ',' '.')
```

Kemudian, untuk path sasaran yang akan dimonitor (juga ditampilkan dalam file agregasi) kami deklarasikan variabel `$path` yang berisi **/home/kali**.

```bash
path="/home/kali"
```

Setelah itu, untuk mendapatkan timestamp untuk format nama file agregasi, kami menggunakan date pada bash dan disimpan pada variable **$timeStamp**.
```bash
timeStamp=$(date +"%Y%m%d%H")
```

Selanjutnya untuk tiap data minimun, maximum, dan average kami tampung pada variabel masing-masing.

```bash
min="minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$path,$path_size_min"
max="minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$path,$path_size_min"
avg="average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$path,$path_size_avg"
```

Data yang disimpan pada variabel berikutlah yang akan menjadi isi dari file agregasi yang tergenerate

```bash
echo $min > /home/kali/log/metrics_agg_$timeStamp.log
```

Kemudian, agar file berjalan secara otomatis setiap satu jam, kami menggunakan cron sebagai berikut
```
*/60 * * * * bash /home/kali/soal-shift-sisop-modul-1-it03-2022/soal3/aggregate_minutes_to_hourly_log.sh
```
