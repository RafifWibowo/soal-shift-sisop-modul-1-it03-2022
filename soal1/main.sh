#!/bin/bash

log_path=./log.txt
db_path=./users/user.txt

read -p "username: " username
read -s -p "password: " password

user=$(grep "$username" $db_path)

if [ "$user" != "" ]; then
    correct_pass="$(echo $user | awk '{print $2}')"
    if [ $correct_pass != $password ]; then
        message="LOGIN: ERROR Failed login attempt on user $username"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
        exit 1
    elif [ $correct_pass == $password ]; then
    	message="LOGIN: INFO User $username logged in"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
    fi
fi

echo -e "\n"
read -p "Insert command (dl, att): " command

case "$command" in
	"dl")
		echo -e "command belum di develop\n"
		;;
	"att")
		successLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($6 == var) n+=1} END{print n-1}' log.txt)
		failedLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($NF == var) n+=1} END{print n}' log.txt)
		#echo -e "$successLog $failedLog\n"
		sum=$(($successLog+$failedLog))
		echo -e "Attempts: $sum \n"
		;;
	*)
		echo -e "Command doesn't exist!"
		exit 1
		;;
	esac
