#!/bin/bash
log_path="log.txt"
dir=./users
db_path=./users/user.txt
if [ -d "$dir" ]; then
	:
else
	mkdir ./users
	touch ./users/user.txt
fi

#A
read -p "username: " username
read -s -p "password: " password

data=$(awk -v var="$username" '$1~var{print $1}' $db_path)
if [ -n "$data" ]
then
	echo -e "\n$username exist!"
    message="REGISTER: ERROR User already exists $username"
    echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
    exit 1
fi 


#B
echo -e "\n"
checkpass(){
    # $1 = username $2 = password
    if  [ $1 == $2 ]
        then echo -e "Requirment 1 is not enough\n"
        return 0
    elif [ ${#2} -lt 8 ]
        then echo -e "Requirment 2 is not enough\n"
        return 0
    else 
        if [[ $2 =~ ^[[:alnum:]]+$ ]];then
			:
                else
                        echo -e "Requirement is not Enough\n"
                        return 0
	fi
    fi
    echo $1 $2 >> $db_path
    message="REGISTER: INFO User $username registered successfully"
    echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
}
checkpass $username $password
