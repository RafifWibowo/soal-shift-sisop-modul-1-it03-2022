DIR=/home/kali/log
if [ ! -d "$DIR" ]; then
	mkdir /home/kali/log
else
	:
fi
ram=$(eval "free -m")
storage=$(eval "du -sh /home/kali")
dataRam=(${ram//:/ })
dataStorage=(${storage//:/ })
mem_total=${dataRam[7]}
mem_used=${dataRam[8]}
mem_free=${dataRam[9]}
mem_shared=${dataRam[10]}
mem_available=${dataRam[12]}
swap_total=${dataRam[14]}
swap_used=${dataRam[15]}
swap_free=${dataRam[16]}
path=${dataStorage[1]}
path_size=${dataStorage[0]}
timeStamp=$(date +"%Y%m%d%H%M%S")
log="$mem_total,$mem_used,$mem_free,$mem_shared,$mem_available,$swap_total,$swap_used,$swap_free,$path,$path_size"
echo "$log" > /home/kali/log/metrics_$timeStamp.log
